Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: open3d
Source: https://open3d.org
Files-Excluded:
 3rdparty/assimp
 3rdparty/azure_kinect
 3rdparty/benchmark
 3rdparty/boringssl
 3rdparty/civetweb
 3rdparty/clang-format
 3rdparty/cub
 3rdparty/curl
 3rdparty/cutlass
 3rdparty/dirent
 3rdparty/eigen
 3rdparty/embree
 3rdparty/filament
 3rdparty/fmt
 3rdparty/glew
 3rdparty/glfw
 3rdparty/googletest
 3rdparty/imgui
 3rdparty/ippicv
 3rdparty/jsoncpp
 3rdparty/libjpeg-turbo
 3rdparty/liblzf
 3rdparty/libpng
 3rdparty/librealsense
 3rdparty/mkl
 3rdparty/msgpack
 3rdparty/nanoflann
 3rdparty/openblas
 3rdparty/parallelstl
 3rdparty/pybind11
 3rdparty/qhull
 3rdparty/stdgpu
 3rdparty/tinygltf
 3rdparty/tinyobjloader
 3rdparty/webrtc
 3rdparty/zeromq
 3rdparty/zlib
Files-Excluded-PoissonRecon:
 JPEG
 PNG
 ZLIB
 *.vcxproj
 *.sln
Comment:
 - Removed source code of libraries which are packaged in Debian already
 - Removed Visual Studio project files which are not needed for Debian

Files: *
Copyright: 2018-2022, www.open3d.org
           2017-2018, Apple Inc
           2011, Carl Rogers
           1998, Hewlett-Packard Company
           2018, Google Inc
           2020-2021, Ignacio Vizzo, Cyrill Stachniss, University of Bonn
           Microsoft Corporation
           2019, Saman Ashkiani
           2019, Shaoshuai Shi
           2018, The Android Open Source Project
           2016, The WebRTC project authors
           2018, Zhijian Liu, Haotian Tang, Yujun Lin
License: Expat

Files: PoissonRecon/*
Copyright: 2006-2019, Michael Kazhdan and Matthew Bolitho
License: Expat

Files: 3rdparty/rply/*
Copyright: 2003-2013, Diego Nehab
License: Expat

Files: cpp/open3d/visualization/gui/Resources/Roboto-Bold.ttf
       cpp/open3d/visualization/gui/Resources/Roboto-BoldItalic.ttf
       cpp/open3d/visualization/gui/Resources/Roboto-Medium.ttf
       cpp/open3d/visualization/gui/Resources/Roboto-MediumItalic.ttf
       cpp/open3d/visualization/gui/Resources/RobotoMono-Medium.ttf
Copyright: 2011, Christian Robertson
           2011, Google Inc
           2015, The Roboto Mono Project Authors (https://github.com/googlefonts/robotomono)
License: Apache-2

Files: 3rdparty/uvatlas/*
Copyright: 2011-2022, Microsoft Corp
           2014-2022, Microsoft Corporation
License: Expat

Files: 3rdparty/tinyfiledialogs/*
Copyright: 2014-2016, Guillaume Vareille
License: Zlib

Files: 3rdparty/tomasakeninemoeller/*
Copyright: Tomas Akenine-Moeller
License: public-domain

Files: PoissonRecon/Src/MyMiscellany.h
Copyright: 2017, Michael Kazhdan
License: CC-BY-3

Files: debian/*
Copyright: 2021-2022, Timo Röhling <roehling@debian.org>
License: Expat

License: Apache-2
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 .
     http://www.apache.org/licenses/LICENSE-2.0
 .
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 .
 On Debian systems, the full license text is also available in
 /usr/share/common-licenses/Apache-2.0

License: CC-BY-3
 Redistribution and use in source and binary forms, with or without modification,
 are permitted provided that the following conditions are met:
 .
 Redistributions of source code must retain the above copyright notice, this list of
 conditions and the following disclaimer. Redistributions in binary form must reproduce
 the above copyright notice, this list of conditions and the following disclaimer
 in the documentation and/or other materials provided with the distribution. 
 .
 Neither the name of the Johns Hopkins University nor the names of its contributors
 may be used to endorse or promote products derived from this software without specific
 prior written permission. 
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
 EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO THE IMPLIED WARRANTIES 
 OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
 SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
 TO, PROCUREMENT OF SUBSTITUTE  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
 DAMAGE.

License: Expat
 Permission is hereby granted, free of charge, to any person obtaining a
 copy of this software and associated documentation files (the "Software"),
 to deal in the Software without restriction, including without limitation
 the rights to use, copy, modify, merge, publish, distribute, sublicense,
 and/or sell copies of the Software, and to permit persons to whom the
 Software is furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included
 in all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

License: Zlib
 This software is provided 'as-is', without any express or implied
 warranty. In no event will the authors be held liable for any damages
 arising from the use of this software.
 .
 Permission is granted to anyone to use this software for any purpose,
 including commercial applications, and to alter it and redistribute it
 freely, subject to the following restrictions:
 .
 1. The origin of this software must not be misrepresented; you must not
    claim that you wrote the original software. If you use this software
    in a product, an acknowledgment in the product documentation would be
    appreciated but is not required.
 2. Altered source versions must be plainly marked as such, and must not be
    misrepresented as being the original software.
 3. This notice may not be removed or altered from any source distribution.

License: public-domain
 Code from: http://fileadmin.cs.lth.se/cs/Personal/Tomas_Akenine-Moller/code/
 Dedicated to the public domain by the author

